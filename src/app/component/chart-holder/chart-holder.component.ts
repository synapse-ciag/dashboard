import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { RenderedChart } from 'src/app/app.component';

@Component({
  selector: 'app-chart-holder',
  templateUrl: './chart-holder.component.html',
  styleUrls: ['./chart-holder.component.scss']
})

export class ChartHolderComponent implements OnInit, OnChanges {


  @Input()
  charts: Array<RenderedChart>;
  colSize: number;

  constructor() {}

  ngOnInit() {
    this.colSize = 12;
  }

  columnSelected(index, renderedChart: RenderedChart, cIndex) {
    const chartTransformer = renderedChart.chartTransformer;
    renderedChart.selectedIndex = index;
    this.charts[cIndex] = JSON.parse(JSON.stringify(renderedChart));
    this.charts[cIndex].chartTransformer = chartTransformer;
  }

  plusChartSize() {
    if (this.colSize === 12) {
      console.log('max size');
    } else {
      this.colSize += 1;
      console.log(this.colSize);
    }
  }

  minosColSize() {
    if (this.colSize === 3) {
      console.log('minimun size');
    } else {
      this.colSize -= 1;
      console.log(this.colSize);
    }
  }

  ngOnChanges() {
    console.log(`changes`);
  }
}
