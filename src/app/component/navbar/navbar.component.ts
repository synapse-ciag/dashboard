import { Component, OnInit, Output, EventEmitter } from '@angular/core';

export interface MenuItem {
  name: string;
  type: string;
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Output()
  menuClick: EventEmitter<MenuItem> = new EventEmitter();

  toggleMenu = true;

  public menuItems: Array<MenuItem>;

  constructor() {
    this.menuItems = [
      { name : 'Lines', type: 'line' },
      { name: 'Bars', type: 'bar' },
      { name: 'Pie', type: 'pie' },
    ];
  }

  ngOnInit() {
  }

  onMenuClick(menuItem) {
    this.menuClick.emit(menuItem);
  }

}
